import os

liste_m1 = []
liste_m2 = []
liste_com = []

def distance_hamming(mot1,mot2):

    dist_ham = 0
    m1=mot1.upper()
    m2=mot2.upper()

    if len(m1) == len(m2):
        for i in range(0,len(m1),1):
           if m1[i]!=m2[i]:
                dist_ham = dist_ham+1
                liste_m1.append(m1[i])
                liste_m2.append(m2[i])
           else:
               liste_com.append(m1[i])

        return dist_ham

    else:
        print("La longueur des mots n'est pas la même")


m1 = input("Saisissez un premier mot : ")
m2 = input("Saisissez un deuxième mot : ")

resultat = distance_hamming(m1, m2)

print("La distance de hamming pour les mots {} et {} est de {}".format(m1.upper(), m2.upper(), resultat))
print("Lettre(s) différente(s) dans le mot {} : {} ".format(m1.upper(), liste_m1).__str__().replace('[', '').replace(']', ''))
print("Lettre(s) différente(s) dans le mot {} : {} ".format(m2.upper(), liste_m2).__str__().replace('[', '').replace(']', ''))
print("Lettre(s) en commun dans les deux mots : {} ".format(liste_com).__str__().replace('[', '').replace(']', ''))

