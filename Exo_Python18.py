import os


def trouver(part1,part2, adn):
    index = 0
    while index < len(adn):
        if part1 in adn:
            if part2 in adn:
                if 'CATATGC' in adn:
                    return False
                else:
                    return True
            else:
                return False
        else:
            return False


brin1 = input("Saisissez le brin d'ADN numéro 1 : ")
brin2 = input("Saisissez le brin d'ADN numéro 2 : ")
adn = input("Saisissez la séquence ADN du suspect : ")

rep = trouver(brin1.upper(), brin2.upper(), adn.upper())
if rep:
    print("Le suspect est coupable !")
else:
    print("Le suspect est innocent")
