import os


def palindrome(chaine):
    i, longueur = 0, len(chaine)

    while i < longueur:
        if chaine[i] != chaine[-i - 1]:
            return False
        i += 1

    return True


rep = True

while rep:
    mot = input("Saisissez un mot : ")
    if palindrome(mot):
        print("Ce mot est un palindrome.")
    else:
        print("Ce mot n'est pas un palindrome.")

    cont = input("Voulez-vous recommencer? (O/N) : ")
    if cont.upper() == 'O':
        rep = True
    else:
        rep = False
